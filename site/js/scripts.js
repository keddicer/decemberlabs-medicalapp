$(document).ready(function(){

    /* ------------------------------------------- user scroll + responsive menu */

    $(window).scroll(function(){
        if($(document).scrollTop() != 0) {
            $('.header').addClass('menuScroll');
        }
    });

    $(window).scroll(function(){
        if($(document).scrollTop() == 0) {
            $('.header').removeClass('menuScroll');
        }
    });

    function scrollSectionBySection() {

        var windowWith = $(window).width();

        if(windowWith < 600) {
            $('.header').addClass('menuResponsive close');
            $('.responsiveIcon').css('display', 'block');
        }

        if(windowWith > 600) {
            $('.header').removeClass('menuResponsive close');
            $('.responsiveIcon').css('display', 'none');
        }

    };

    scrollSectionBySection();

    function responsiveMenu() {

        var windowWith = $(window).width();

        if(windowWith < 600) {
            $('.header').addClass('menuResponsive close');
            $('.responsiveIcon').css('display', 'block');
        }

        if(windowWith > 600) {
            $('.header').removeClass('menuResponsive close');
            $('.responsiveIcon').css('display', 'none');
        }

    };

    responsiveMenu();

    $(window).resize(function() {
        responsiveMenu();
    });

    function animationMenu() {
        $('.responsiveIcon').toggleClass('closeMenu');
        $('.header').toggleClass('open');
    };

    $('.responsiveIcon').on('click', function(){
        animationMenu();
    });

    $('.link').on('click', function(){
        animationMenu();
    });

    /* ------------------------------------------- sections scroll */

    $('a.scroll').click(function(e){
        e.preventDefault();
        $('html, body').stop().animate({scrollTop: $($(this).attr('href')).offset().top-50}, 1000);
    });

    var waypoint = new Waypoint({
        element: document.getElementById('home'),
        handler: function() {
            $("#homeLnk").addClass("selected");
            $("#teamLnk").removeClass("selected");
            $("#projectsLnk").removeClass("selected");
            $("#servicesLnk").removeClass("selected");
            $("#contactLnk").removeClass("selected");
        },
        offset: -20
    });

    var waypoint = new Waypoint({
        element: document.getElementById('team'),
        handler: function() {
            $("#teamLnk").addClass("selected");
            $("#homeLnk").removeClass("selected");
            $("#projectsLnk").removeClass("selected");
            $("#servicesLnk").removeClass("selected");
            $("#contactLnk").removeClass("selected");
        },
        offset: 55
    });

    var waypoint = new Waypoint({
        element: document.getElementById('projects'),
        handler: function() {
            $("#projectsLnk").addClass("selected");
            $("#homeLnk").removeClass("selected");
            $("#teamLnk").removeClass("selected");
            $("#servicesLnk").removeClass("selected");
            $("#contactLnk").removeClass("selected");
        },
        offset: 55
    });

    var waypoint = new Waypoint({
        element: document.getElementById('services'),
        handler: function() {
            $("#servicesLnk").addClass("selected");
            $("#homeLnk").removeClass("selected");
            $("#teamLnk").removeClass("selected");
            $("#projectsLnk").removeClass("selected");
            $("#contactLnk").removeClass("selected");
        },
        offset: 0
    });

    var waypoint = new Waypoint({
        element: document.getElementById('contact'),
        handler: function() {
            $("#contactLnk").addClass("selected");
            $("#homeLnk").removeClass("selected");
            $("#teamLnk").removeClass("selected");
            $("#projectsLnk").removeClass("selected");
            $("#servicesLnk").removeClass("selected");
        },
        offset: 0
    });

    /* ------------------------------------------- link selected */

    $('.menu .link').on('click', function(){
        $('.menu .link').removeClass('selected');
        $(this).addClass('selected');
    });

    /* ------------------------------------------- slider */

    function selectNavigationItem(index, name) {
	$('.navigation').children().children().children().removeClass('selected');
	$('#' + index).addClass('selected');

	$('.item').css('opacity', '0');
        $('#' + name).css('opacity', '1');

        $('.mobileCommerceBack').css('opacity', (index == 'one') ? '1' : '0');
        $('.cuttingEdgeBack').css('opacity', (index == 'two') ? '1' : '0');
        $('.financeBack').css('opacity', (index == 'three') ? '1' : '0');
        $('.musicBack').css('opacity', (index == 'four') ? '1' : '0');
    };

    $('#one').on('click', function(){
        selectNavigationItem('one', 'itemOne');
    });

    $('#two').on('click', function(){
        selectNavigationItem('two', 'itemTwo');
    });

    $('#three').on('click', function(){
        selectNavigationItem('three', 'itemThree');
    });

    $('#four').on('click', function(){
        selectNavigationItem('four', 'itemFour');
    });

    $('#itemOne .mobileNavigation .left').on('click', function(){
        selectNavigationItem('four', 'itemFour');
    });

    $('#itemOne .mobileNavigation .right').on('click', function(){
        selectNavigationItem('two', 'itemTwo');
    });

    $('#itemTwo .mobileNavigation .left').on('click', function(){
        selectNavigationItem('one', 'itemOne');
    });

    $('#itemTwo .mobileNavigation .right').on('click', function(){
        selectNavigationItem('three', 'itemThree');
    });

    $('#itemThree .mobileNavigation .left').on('click', function(){
        selectNavigationItem('two', 'itemTwo');
    });

    $('#itemThree .mobileNavigation .right').on('click', function(){
        selectNavigationItem('four', 'itemFour');
    });

    $('#itemFour .mobileNavigation .left').on('click', function(){
        selectNavigationItem('three', 'itemThree');
    });

    $('#itemFour .mobileNavigation .right').on('click', function(){
        selectNavigationItem('one', 'itemOne');
    });


});
